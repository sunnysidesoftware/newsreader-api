# UiPath Implementation

## Workflow

The main workflow of the newsbot is essentially a bot googler: it opens your computer's browser, goes to Google News, and takes an input from the queue - the keyword of which type of news you are looking for.
The bot locates the searchbar of the Google News website and enters the queued input. Then it presses ENTER to search.
In the spirit of simplicity, the bot takes the first piece of news that comes up, opens its website and copies down the title, as well as the first paragraph into a local table.
(This is set up so that you can loop the previous step and acquire as many news pieces as you want.)
The bot reads the title. Finally, the tables contents along with appropriate URLs are stored in a local .csv file for future reference.

## Orchestrator

The orchestrator is set up with the bot's personal queue, which takes items sent from our (at that time running) local host, and puts them up into a queue.
The bot then takes these items for its googling process.

## Integration with server

The UiPath bot is then integrated with a simple Node express.js app. The app takes in input from the user in the form of audio, and then uses a speech-to-text IBM API to transform it into a query. This query is then sent to the UiPath bot, which returns the relevant news items that are promptly read to the user using speech-to-text. The code for the API can be found in the `server.js` file.