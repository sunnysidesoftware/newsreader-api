
const express = require('express');
const bodyparser = require('body-parser');
const multer = require('multer');
const cors = require('cors');
const fs = require('fs');
const axios = require('axios');
const { execSync } = require('child_process');
const path = require('path');

let config = {};

// configure storage on disk for each user.
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let dir = './uploads/' + req.params.randomKey;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    };
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(null, req.params.randomMessageKey + ".ogg");
  }
});

let authenticateUIPath = () => {
  return new Promise((resolve, reject) => {
    axios.post('https://platform.uipath.com/api/account/authenticate', {
      "tenancyName" : "NewsBotTenant",
      "usernameOrEmailAddress" : "mikulas.plesak@gmail.com",
      "password" : "ycyaspm0"
    }).then(resp => {
      let key = resp.data.result;
      config.uikey = key;
      resolve();
    });
  });
};

let authenticateIBM = () => {
  return new Promise((resolve, reject) => {
    const SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');

    const speechToText = new SpeechToTextV1({
        iam_apikey: 'MsgOfKAQy7VNiwQF-rEjx9Sc39ylXdA7vfreOb8lBO_e',
        url: 'https://stream.watsonplatform.net/speech-to-text/api'
    });

    resolve(speechToText);
  });
}

let convertFile = (name) => {
  let comm = "ffmpeg -i " + name + " " + name.slice(0, -4) + ".wav";
  // convert to sync. YOU NEED TO HAVE FFMPEG INSTALLED
  execSync(comm);
}



let addToTransaction = (input) => {
  return new Promise((resolve, reject) => {
    axios.post('https://platform.uipath.com/odata/Queues/UiPathODataSvc.AddQueueItem',
      {
        "itemData": {
      		"Priority": "High",
      		"Name": "NewsBot",
      		"SpecificContent": {
      			"NewsInput@odata.type": "#String",
      			"NewsInput": input
      		}
      	}
      },
      {
        headers: {
          Authorization: "Bearer " + config.uikey
        }
      }
    ).then(resp => {
      //console.log(resp);
      resolve(resp);
    }).catch(err => {
      //console.error(err);
      reject(err);
    });
  });
};

let getEnvironmentKey = () => {
  return new Promise((resolve, reject) => {
    axios.get("https://platform.uipath.com/odata/Environments/UiPath.Server.Configuration.OData.GetRobotIdsForEnvironment(key=NewsEnvironment)").then(res => {
      resolve()
    }).catch(err => {
      reject(err);
    });
  });
};

let startJob = () => {
  return new Promise((resolve, reject) => {
    axios.post('https://platform.uipath.com/odata/Jobs/UiPath.Server.Configuration.OData.StartJobs',
    {
      "startInfo": {
        "ReleaseKey": "0b8c5e0c-f46e-4d99-a428-59b4a79d857a",
        "Strategy": "All",
        "RobotIds": []
      }
    },
    {
      headers: {
        Authorization: "Bearer " + config.uikey
      }
    }).then((res) => {
      console.log(res);
      resolve(res);
    }).catch(err => {
      console.error(err);
    });
  });
};


let upload = multer({ storage: storage });


let app = express();
app.use(cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// listen for voice inputs.
app.post("/api/upload/:randomKey/:randomMessageKey", upload.single('command'), function (req, res) {

    authenticateUIPath().then(() => {
      console.log("UIPath authenticated.");
      return authenticateIBM();
    }).then(speechToText => {
      console.log("IBM authenticated.");
      convertFile('./uploads/' + req.params.randomKey + '/' + req.params.randomMessageKey + '.ogg');
      console.log("File converted.");
      // obm config
      let IBMconfig = {
        audio: fs.createReadStream('./uploads/' + req.params.randomKey + '/' + req.params.randomMessageKey + '.wav'),
        content_type: 'audio/wav',
        timestamps: true
      };

      speechToText.recognize(IBMconfig, (err, result) => {
        if (err) {
          console.error(err);
          return;
        }
        console.log("IBM translated.");
        let transcript = result.results[0].alternatives[0].transcript;
        transcript = transcript.replace(" ", "%20");
        return addToTransaction(transcript).then(res => {
          console.log("Added to transaction");
          return startJob();
        }).then(res => {
          console.log("Job has run!")
        }).catch(err => {
          console.log(err);
        });

      });
    });

});

app.post("/api/receive", (req, res) => {

});

app.listen(8080, () => {
  console.log("Server running...");
});
